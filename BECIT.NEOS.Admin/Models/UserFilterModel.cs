﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class UserFilterModel
    {
        public string userid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public DateTime? lastLoginFrom { get; set; }
        public DateTime? lastLoginTo { get; set; }
    }
}