﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class UserModel
    {
        public string userid { get; set; }
        public string title { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public DateTime? lastLogin { get; set; }
        public string AppUsreRoleCode { get; set; }
        public string AppUsreRoleDesc { get; set; }
    }
}