﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserController : _BaseController
    {
        //
        // GET: /User/

        public ActionResult Index(int? page,string keyword)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                //var objModel = (from q in db.APPUSER select q).ToList();
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    userid = q.USERID,
                                    title = q.TITLE,
                                    fname = q.FNAME,
                                    lname = q.LNAME,
                                    lastLogin = q.LASTLOGIN,
                                    AppUsreRoleCode = q.APPUSERROLECODE,
                                    AppUsreRoleDesc = q.APPUSERROLE.DESCRIPTION
                                }).ToList();
                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.userid.ToLower().Contains(keyword.ToLower()) || x.fname.ToLower().Contains(keyword.ToLower())).ToList();
                ViewBag.keyword = keyword;

                int pageSize = 2;
                int pageNumber = (page ?? 1);
                var listPaged = objModel.ToPagedList(pageNumber,pageSize);
                return View(listPaged);
            }
            
        }
        [HttpPost]
        public ActionResult Index(string keyword,string dummy)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            return Index(1,keyword);
        }

        public ActionResult InputForm(string id)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            var objModel = new UserModel();
            if(id != null)
            {
                using (EntitiesNeosDb db = new EntitiesNeosDb())
                {
                    var dbObj = (from q in db.APPUSER where q.USERID == id select q).FirstOrDefault();
                    objModel.userid = dbObj.USERID;
                    objModel.title = dbObj.TITLE;
                    objModel.fname = dbObj.FNAME;
                    objModel.lname = dbObj.LNAME;
                    objModel.email = dbObj.EMAIL;
                }
            }
            return View(objModel);
        }

        [HttpPost]
        public ActionResult InputForm(UserModel objModel)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSER";
            //Insert
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                try
                {
                    var countExistRecord = (from q in db.APPUSER where q.USERID == objModel.userid select q).Count();

                    if (countExistRecord == 0)
                    {
                        var dbObject = new APPUSER();
                        dbObject.USERID = objModel.userid;
                        dbObject.TITLE = objModel.title;
                        dbObject.FNAME = objModel.fname;
                        dbObject.LNAME = objModel.lname;
                        dbObject.EMAIL = objModel.email;
                        dbObject.APPUSERROLECODE = objModel.AppUsreRoleCode;

                        db.APPUSER.Add(dbObject);
                        db.SaveChanges();
                    }
                    else
                    {
                        var existObject = (from q in db.APPUSER where q.USERID == objModel.userid select q).FirstOrDefault();
                        existObject.TITLE = objModel.title;
                        existObject.FNAME = objModel.fname;
                        existObject.LNAME = objModel.lname;
                        existObject.EMAIL = objModel.email;
                        db.SaveChanges();
                    }
                    var messageResult = new MessageResultModel();
                    messageResult.Style = "alert alert-success";
                    messageResult.Title = "Success";
                    messageResult.Description = "บันทึกข้อมูลเรียบร้อย";
                    TempData["MessageResult"] = messageResult;
                }
                catch(Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;
                    var messageResult = new MessageResultModel();
                    messageResult.Style = "alert alert-danger";
                    messageResult.Title = "Error!";
                    messageResult.Description = "เกิดข้อผิดพลาด"+exc.Message;
                    TempData["MessageResult"] = messageResult;
                }
            }


            return View(objModel);
        }


    }
}
