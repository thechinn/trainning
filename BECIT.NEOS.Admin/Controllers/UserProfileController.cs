﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserProfileController : Controller
    {
        //
        // GET: /UserProfile/

        public ActionResult Index()
        {
            CurrentUserModel currentUser = (CurrentUserModel)Session["CurrentUser"];

            ViewBag.fname = currentUser.fname;
            ViewBag.lname = currentUser.lname;
            ViewBag.lastLogin = currentUser.lastLogin;
            return View();
        }

    }
}
