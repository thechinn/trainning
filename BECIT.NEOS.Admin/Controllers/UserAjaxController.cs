﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserAjaxController : _BaseController
    {
        //
        // GET: /UserAjax/

        public ActionResult Index(int? page, string keyword)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSERAJAX";
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                //var objModel = (from q in db.APPUSER select q).ToList();
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    userid = q.USERID,
                                    title = q.TITLE,
                                    fname = q.FNAME,
                                    lname = q.LNAME,
                                    lastLogin = q.LASTLOGIN,
                                    AppUsreRoleCode = q.APPUSERROLECODE,
                                    AppUsreRoleDesc = q.APPUSERROLE.DESCRIPTION
                                }).ToList();
                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.userid.ToLower().Contains(keyword.ToLower()) || x.fname.ToLower().Contains(keyword.ToLower())).ToList();
                ViewBag.keyword = keyword;

                int pageSize = 2;
                int pageNumber = (page ?? 1);
                var listPaged = objModel.ToPagedList(pageNumber, pageSize);
                ViewBag.pagedListObj = listPaged;


                return Request.IsAjaxRequest()?(ActionResult)PartialView("Table") : View();
            }

        }

        [HttpPost]
        public ActionResult Index(string keyword)
        {
            ViewBag.CurrentAppModuleCode = "DEMOUSERAJAX";
            return Index(null, keyword);
        }

    }
}
