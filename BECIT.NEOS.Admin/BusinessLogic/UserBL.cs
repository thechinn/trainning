﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.BusinessLogic
{
    public class UserBL
    {
        public List<UserModel> getAll(string keyword)
        {
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                //var objModel = (from q in db.APPUSER select q).ToList();
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    userid = q.USERID,
                                    title = q.TITLE,
                                    fname = q.FNAME,
                                    lname = q.LNAME,
                                    lastLogin = q.LASTLOGIN,
                                    AppUsreRoleCode = q.APPUSERROLECODE,
                                    AppUsreRoleDesc = q.APPUSERROLE.DESCRIPTION
                                }).ToList();

                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.userid.ToLower().Contains(keyword.ToLower()) || x.fname.ToLower().Contains(keyword.ToLower())).ToList();
                return objModel;

            }
        }
    }
}
